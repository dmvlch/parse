from bs4 import BeautifulSoup as bs 
import requests

headers = {'accept':'*/*',
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0'}

base_url = 'https://www.interactivebrokers.com/en/index.php?f=2222&exch=nasdaq&showcategories=STK&p=&cc=&limit=100&page=1'

ticker_list = []

def ib_parse_nasdaq(base_url, headers):
    session = requests.Session()
    request = session.get(base_url, headers=headers)

    if request.status_code == 200:
        soup = bs(request.content, 'lxml')

        pages = soup.find('ul', attrs={'class': 'pagination'}).text.strip().split()[-2]
        for i in range(int(pages)):
            url = f'https://www.interactivebrokers.com/en/index.php?f=2222&exch=nasdaq&showcategories=STK&p=&cc=&limit=100&page={i+1}'
            request = session.get(url, headers=headers)
            soup = bs(request.content, 'lxml')

            full_table = soup.find('div', attrs={'class': 'table-responsive no-margin'}).find('tbody').find_all('tr')
            for i in range(len(full_table)):
                tickers = full_table[i].text.strip().split('\n')
                ticker_list.append({
                    tickers[0]:tickers[1]
                    })

            print(len(ticker_list))
            #print(ticker_list)

    else:
        print('ERROR')
    
ib_parse_nasdaq(base_url, headers)

